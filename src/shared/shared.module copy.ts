import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
// import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import { configModuleOptions } from './configs/module-options';
import { APP_FILTER } from '@nestjs/core';
import { AllExceptionsFilter } from './filters/all-exceptions.filter';
// import { AllExceptionsFilter } from './filters/all-exceptions.filter';
// import { LoggingInterceptor } from './interceptors/logging.interceptor';
// import { AppLoggerModule } from './logger/logger.module';

@Module({
  imports: [
    ConfigModule.forRoot(configModuleOptions),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: configService.get<string>('DB.TYPE') as
          | 'mysql'
          | 'mariadb'
          | undefined,
        host: configService.get<string>('DB.HOST'),
        port: configService.get<number | undefined>('DB.PORT') || 3306,
        database: configService.get<string>('DB.NAME'),
        username: configService.get<string>('DB.USER'),
        password: configService.get<string>('DB.PASS'),
        entities: [__dirname + '/../**/entities/*.entity{.ts,.js}'],
        // Timezone configured on the Postgres server.
        // This is used to typecast server date/time values to JavaScript Date object and vice versa.
        timezone: 'Z',
        synchronize: false,
        debug: configService.get<string>('env') === 'development',
      }),
    }),
    // AppLoggerModule,
  ],
  // exports: [AppLoggerModule, ConfigModule],
  exports: [ConfigModule],
  providers: [
    // { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor },
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
})
export class SharedModule {}
