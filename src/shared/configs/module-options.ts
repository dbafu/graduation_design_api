import { ConfigModuleOptions } from '@nestjs/config/dist/interfaces';
import * as Joi from 'joi';

import configuration from './configuration';

export const configModuleOptions: ConfigModuleOptions = {
  // envFilePath: '.env',
  load: [configuration],
  // validationSchema: Joi.object({
  //   ['APP.ENV']: Joi.string()
  //     .valid('development', 'production', 'test')
  //     .default('development'),
  //   ['APP.PORT']: Joi.number().required(),
  //   ['APP.DEFAULT_ADMIN_USER_PASSWORD']:
  //   ['DB.TYPE']: Joi.string()
  //     .valid(
  //       'mysql',
  //       'postgres',
  //       'mariadb',
  //       'sqlite',
  //       'better-sqlite3',
  //       'cordova',
  //       'oracle',
  //       'mssql',
  //       'mongodb',
  //       'sqljs',
  //     )
  //     .default('mysql'),
  //   ['DB.HOST']: Joi.string().required(),
  //   ['DB.PORT']: Joi.number().optional(),
  //   ['DB.NAME']: Joi.string().required(),
  //   ['DB.USER']: Joi.string().required(),
  //   ['DB.PASS']: Joi.string().required(),
  //   ['JWT.PUBLIC_KEY_BASE64']: Joi.string().required(),
  //   ['JWT.PRIVATE_KEY_BASE64']: Joi.string().required(),
  //   ['JWT.ACCESS_TOKEN_EXP_IN_SEC']: Joi.number().required(),
  //   ['JWT.REFRESH_TOKEN_EXP_IN_SEC']: Joi.number().required(),
  // }),
  validationSchema: Joi.object({
    APP: {
      ENV: Joi.string()
        .valid('development', 'production', 'test')
        .default('development'),
      PORT: Joi.number().required(),
      DEFAULT_ADMIN_USER_PASSWORD: Joi.string().required(),
    },
    DB: {
      TYPE: Joi.string()
        .valid(
          'mysql',
          'postgres',
          'mariadb',
          'sqlite',
          'better-sqlite3',
          'cordova',
          'oracle',
          'mssql',
          'mongodb',
          'sqljs',
        )
        .default('mysql'),
      HOST: Joi.string().required(),
      PORT: Joi.number().optional(),
      NAME: Joi.string().required(),
      USER: Joi.string().required(),
      PASS: Joi.string().required(),
    },
    JWT: {
      PUBLIC_KEY_BASE64: Joi.string().required(),
      PRIVATE_KEY_BASE64: Joi.string().required(),
      ACCESS_TOKEN_EXP_IN_SEC: Joi.number().required(),
      REFRESH_TOKEN_EXP_IN_SEC: Joi.number().required(),
    },
  }),
};
