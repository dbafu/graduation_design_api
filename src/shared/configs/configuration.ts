import { readFileSync } from 'fs';
import * as yaml from 'js-yaml';
import { join } from 'path';

const YAML_CONFIG_FILENAME = 'config.yaml';

export default () => {
  return yaml.load(
    readFileSync(join(__dirname, YAML_CONFIG_FILENAME), 'utf8'),
  ) as Record<string, any>;
};

// console.log(
//   yaml.load(readFileSync(join(__dirname, YAML_CONFIG_FILENAME), 'utf8')),
// );

/**
{
  APP: { ENV: 'development', PORT: 5000 },
  DB: {
    TYPE: 'mysql',
    HOST: 'localhost',
    PORT: 3308,
    NAME: 'day_book',
    USER: 'root',
    PASS: 123456
  },
  JWT: {
    ACCESS_TOKEN_EXP_IN_SEC: 3600,
    REFRESH_TOKEN_EXP_IN_SEC: 7200,
    PUBLIC_KEY_BASE64: 'PUBLIC_KEY_BASE64',
    PRIVATE_KEY_BASE64: 'PRIVATE_KEY_BASE64',
    DEFAULT_ADMIN_USER_PASSWORD: 'admin-example'
  }
}
*/
