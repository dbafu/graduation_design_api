// import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
// import { Observable } from 'rxjs';
import axios from 'axios';
const key = 'a546dd6657824b6882ec461255642293';
@Injectable()
export class AppService {
  // constructor(private readonly httpService: HttpService) {}

  getHello(): string {
    return 'Hello World!';
  }

  // https://geoapi.qweather.com/v2/city/lookup?location=zhoukou&key=a546dd6657824b6882ec461255642293
  async getWeather(location: string) {
    const res = await axios.get(
      `https://geoapi.qweather.com/v2/city/lookup?location=${location}&key=${key}`,
    );

    console.log('---ftf res: ', res.data);
    if (res.status === 200 && Array.isArray(res.data.location))
      return res.data.location;
  }
}
