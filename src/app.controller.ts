import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('weather/:city')
  getWeather(@Param('city') city: string) {
    return this.appService.getWeather(city);
  }
}
