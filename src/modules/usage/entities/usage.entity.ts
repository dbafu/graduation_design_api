import { Finance } from 'src/modules/finance/entities/finance.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  Unique,
  OneToMany,
  Entity,
} from 'typeorm';

export enum TurnoverType {
  INFLOW = 'Inflow', // 流入
  EXPENDITURE = 'Expenditure', // 支出
}

@Entity('usages')
export class Usage {
  @PrimaryGeneratedColumn()
  id: number;

  @Unique('name', ['name'])
  @Column({ length: 200 })
  name: string;

  // @Unique('icon', ['icon'])
  @Column({ length: 200 })
  icon: string;

  @Column({ length: 200, default: null })
  iconRemote: string;

  // @Unique('order', ['order'])
  @Column({ type: 'int', default: -1 })
  order: number;

  @OneToMany(() => Finance, (finance) => finance.usage)
  finances: Finance[];
}
