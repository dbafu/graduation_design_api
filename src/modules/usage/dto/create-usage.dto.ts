import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateUsageDto {
  @IsNotEmpty()
  @ApiProperty()
  @IsString()
  @MaxLength(200)
  name: string;

  @IsNotEmpty()
  @ApiProperty()
  @IsString()
  icon: string;
}
