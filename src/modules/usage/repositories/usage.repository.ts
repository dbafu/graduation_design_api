import { Injectable, NotFoundException } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

import { Usage } from '../entities/usage.entity';

@Injectable()
export class UsageRepository extends Repository<Usage> {
  constructor(private dataSource: DataSource) {
    super(Usage, dataSource.createEntityManager());
  }

  async getById(id: number): Promise<Usage> {
    const usage = await this.findOne({ where: { id } });
    if (!usage) {
      throw new NotFoundException();
    }

    return usage;
  }
}
