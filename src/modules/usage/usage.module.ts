import { Module } from '@nestjs/common';
import { UsageService } from './services/usage.service';
import { UsageController } from './controllers/usage.controller';
import { UsageRepository } from './repositories/usage.repository';

@Module({
  controllers: [UsageController],
  providers: [UsageService, UsageRepository],
})
export class UsageModule {}
