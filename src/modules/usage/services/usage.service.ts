import { Injectable } from '@nestjs/common';
import { CreateUsageDto } from '../dto/create-usage.dto';
import { UpdateUsageDto } from '../dto/update-usage.dto';
import { UsageRepository } from '../repositories/usage.repository';
import { Usage } from '../entities/usage.entity';
import { plainToInstance } from 'class-transformer';

@Injectable()
export class UsageService {
  constructor(private repository: UsageRepository) {}

  async create(createUsageDto: CreateUsageDto) {
    const usage = plainToInstance(Usage, createUsageDto);

    // this.logger.log(ctx, `calling ${UserRepository.name}.saveUser`);
    await this.repository.save(usage);

    return 'successfull';
  }

  findAll() {
    return this.repository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} usage`;
  }

  update(id: number, updateUsageDto: UpdateUsageDto) {
    return `This action updates a #${id} usage`;
  }

  remove(id: number) {
    return `This action removes a #${id} usage`;
  }
}
