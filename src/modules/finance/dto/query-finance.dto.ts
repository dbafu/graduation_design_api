import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  MaxLength,
  IsEnum,
  // IsNumber,
  IsDate,
  IsBoolean,
  IsOptional,
} from 'class-validator';

import { PaginationParamsDto } from 'src/shared/dtos/pagination-params.dto';
import { TimeRangeType, TurnoverType } from '../enums/finance.enum';
import { Type } from 'class-transformer';

export class QueryFinanceDto extends PaginationParamsDto {
  @IsNotEmpty()
  @ApiProperty()
  @IsString()
  @IsEnum(TurnoverType)
  turnoverType: TurnoverType;

  @ApiProperty()
  @IsBoolean()
  @IsOptional()
  @MaxLength(200)
  isRecordDeleted: Date;

  @ApiProperty()
  @IsDate()
  @Type(() => Date)
  @IsOptional()
  @MaxLength(200)
  start: Date;

  @ApiProperty()
  @IsEnum(TimeRangeType)
  @IsOptional()
  @MaxLength(200)
  timeRangeType: TimeRangeType;

  @ApiProperty()
  @IsDate()
  @Type(() => Date)
  @IsOptional()
  @MaxLength(200)
  end: Date;
}
