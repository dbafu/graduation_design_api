import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  MaxLength,
  IsEnum,
  IsNumber,
  IsDate,
  IsBoolean,
  IsOptional,
} from 'class-validator';
import { TurnoverType } from '../enums/finance.enum';

export class CreateFinanceDto {
  @IsNotEmpty()
  @ApiProperty()
  @IsString()
  @IsEnum(TurnoverType)
  turnoverType: TurnoverType;

  @IsNotEmpty()
  @ApiProperty()
  @IsNumber()
  number: bigint;

  @IsNotEmpty()
  @ApiProperty()
  @IsNumber()
  usageId: number;

  @ApiProperty()
  @IsString()
  @MaxLength(200)
  remark: string;

  @ApiProperty()
  @IsBoolean()
  @IsOptional()
  @MaxLength(200)
  isRecordDeleted: Date;

  @ApiProperty()
  @IsDate()
  @IsOptional()
  @MaxLength(200)
  createdAt: Date;

  @ApiProperty()
  @IsDate()
  @IsOptional()
  @MaxLength(200)
  updatedAt: Date;
}
