export enum TurnoverType {
  INFLOW = 'Inflow', // 流入
  EXPENDITURE = 'Expenditure', // 支出
}

export enum TimeRangeType {
  Week,
  Month,
  Year,
}
