import { Injectable, NotFoundException } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { Finance } from '../entities/finance.entity';

@Injectable()
export class FinanceRepository extends Repository<Finance> {
  constructor(private dataSource: DataSource) {
    super(Finance, dataSource.createEntityManager());
  }

  async getById(id: number): Promise<Finance> {
    const finance = await this.findOne({ where: { id } });
    if (!finance) {
      throw new NotFoundException();
    }

    return finance;
  }
}
