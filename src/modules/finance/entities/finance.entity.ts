import { Usage } from 'src/modules/usage/entities/usage.entity';
import { User } from 'src/modules/user/entities/user.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  Entity,
} from 'typeorm';
import { TurnoverType } from '../enums/finance.enum';

@Entity('finances')
export class Finance {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: TurnoverType,
    default: TurnoverType.EXPENDITURE,
  })
  turnoverType: TurnoverType;

  @Column({ type: 'decimal', precision: 10, scale: 3 })
  number: number;

  @Column({ length: 200 })
  remark: string;

  @Column({
    default: false,
  })
  isRecordDeleted: boolean;

  @ManyToOne(() => User, (user) => user.finances)
  user: User;

  @ManyToOne(() => Usage, (usage) => usage.finances)
  usage: Usage;

  @CreateDateColumn({ name: 'createdAt', nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedAt', nullable: true })
  updatedAt: Date;

  // @OneToMany(() => Article, (article) => article.author)
  // articles: Article[];
}
