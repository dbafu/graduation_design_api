import { Module } from '@nestjs/common';
import { FinanceService } from './services/finance.service';
import { FinanceController } from './controllers/finance.controller';
import { FinanceRepository } from './repositories/finance.repository';

@Module({
  controllers: [FinanceController],
  providers: [FinanceService, FinanceRepository],
})
export class FinanceModule {}
