import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  // Query,
} from '@nestjs/common';
import { FinanceService } from '../services/finance.service';
import { CreateFinanceDto } from '../dto/create-finance.dto';
import { UpdateFinanceDto } from '../dto/update-finance.dto';
import { ReqContext } from 'src/shared/request-context/req-context.decorator';
import { RequestContext } from 'src/shared/request-context/request-context.dto';
import { QueryFinanceDto } from '../dto/query-finance.dto';
import { plainToInstance } from 'class-transformer';

@Controller('finance')
export class FinanceController {
  constructor(private readonly financeService: FinanceService) {}

  @Post()
  create(
    @Body() createFinanceDto: CreateFinanceDto,
    @ReqContext() ctx: RequestContext,
  ) {
    return this.financeService.create(createFinanceDto, ctx);
  }

  // 获取 acc card data
  @Get('accCard')
  getAccCardData(@ReqContext() ctx: RequestContext) {
    return this.financeService.findAccCard(ctx);
  }

  // 获取 acc card list
  @Get('accList')
  findAll(@ReqContext() ctx: RequestContext, @Query() query: QueryFinanceDto) {
    console.log(
      '--findAll---accList-----query-: ',
      plainToInstance(QueryFinanceDto, query),
    );
    query = plainToInstance(QueryFinanceDto, query);

    return this.financeService.findAccList(ctx, query);
  }

  // 获取 sta card 1
  @Get('staCard1')
  findCard1(
    @ReqContext() ctx: RequestContext,
    @Query() query: QueryFinanceDto,
  ) {
    console.log(
      '--findAll---staCard1-----query-: ',
      plainToInstance(QueryFinanceDto, query),
    );
    query = plainToInstance(QueryFinanceDto, query);

    return this.financeService.findCard1(ctx, query);
  }

  // 获取 sta card 2
  @Get('staCard2')
  findCard2(
    @ReqContext() ctx: RequestContext,
    @Query() query: QueryFinanceDto,
  ) {
    console.log(
      '--findAll---staCard2-----query-: ',
      plainToInstance(QueryFinanceDto, query),
    );
    query = plainToInstance(QueryFinanceDto, query);

    return this.financeService.findCard2(ctx, query);
  }

  // 获取 sta card 3
  @Get('staCard3')
  findCard3(
    @ReqContext() ctx: RequestContext,
    @Query() query: QueryFinanceDto,
  ) {
    console.log(
      '--findAll---staCard3-----query-: ',
      plainToInstance(QueryFinanceDto, query),
    );
    query = plainToInstance(QueryFinanceDto, query);

    return this.financeService.findCard3(ctx, query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.financeService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateFinanceDto: UpdateFinanceDto) {
    return this.financeService.update(+id, updateFinanceDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.financeService.remove(+id);
  }
}
