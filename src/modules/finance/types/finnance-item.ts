import { Finance } from '../entities/finance.entity';

export type SortedFinanceItemType = {
  data: Array<Finance>;
  order: number;
  totalInflow?: number;
  totalExpenditure?: number;
  dateKey1: string;
  dateKey2: string;
  dateKey3: string;
};

export type SortedFinanceTempType = Record<string, SortedFinanceItemType>;
