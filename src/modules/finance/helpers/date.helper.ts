import { QueryFinanceDto } from '../dto/query-finance.dto';
import { TimeRangeType } from '../enums/finance.enum';

export const getDayCount = (d1: string | Date, d2: string | Date) => {
  if (typeof d1 === 'string') {
    d1 = new Date(d1);
  }
  if (typeof d2 === 'string') {
    d2 = new Date(d2);
  }

  if (d1 instanceof Date && d2 instanceof Date) {
    const span = d1.getTime() - d2.getTime();

    const dayMilis = 1 * 24 * 3600 * 1000;

    const n = span / dayMilis;

    const res = Math.ceil(n);

    return res;
  }

  throw Error('getDayCount 入参不是日期对象或者日期字符串');
};

export const getLastStartDateObj = (
  start: string | Date,
  timeRangeType: TimeRangeType,
) => {
  if (typeof start === 'string') {
    start = new Date(start);
  }

  if (!(start instanceof Date)) {
    console.log(
      'getLastStartDateObj 入参 start: ' + start + ' 不是日期对象或字符串',
    );
  }

  if (timeRangeType === TimeRangeType.Week) {
    start.setDate(start.getDate() - 7);
  } else if (timeRangeType === TimeRangeType.Month) {
    start.setMonth(start.getMonth() - 1);
  } else if (timeRangeType === TimeRangeType.Year) {
    start.setFullYear(start.getFullYear() - 1);
  }

  return start;
};

const padNum = (v: number) => (v < 10 && v >= 0 ? `0${v}` : v.toString());

const formatDate = (dat: Date) => {
  const y = dat.getFullYear();
  const m = dat.getMonth() + 1;
  const d = dat.getDate();

  return `${y}.${padNum(m)}.${padNum(d)}`;
};

const WEEK_DAY = [
  '星期日',
  '星期一',
  '星期二',
  '星期三',
  '星期四',
  '星期五',
  '星期六',
];

export const dateToKeyStringArr = (dat: Date) => {
  const TODAY_STR = formatDate(new Date());
  const DAT_STR = formatDate(dat);
  const dat_day = dat.getDay();
  // console.log('----------dateToKeyString Date dat: ', dat);
  // console.log('----------dateToKeyString Date TODAY_STR: ', TODAY_STR);
  // console.log('----------dateToKeyString Date DAT_STR: ', DAT_STR);
  // console.log('----------dateToKeyString Date dat_day: ', dat_day);

  const label = DAT_STR === TODAY_STR ? '今天' : WEEK_DAY[dat_day];

  return [`${DAT_STR} ${label}`, DAT_STR, label];
};

export const dateToMysqlDateTimeString = (dat: Date) => {
  if (!(dat instanceof Date)) {
    console.log(
      'dateToMysqlDateTimeString 入参 dat: ' + dat + ' 不是日期对象或字符串',
    );
  }

  return new Date(dat).toISOString().slice(0, 19).replace('T', ' ');
};

export const calculateStartAndEnd = (query: QueryFinanceDto) => {
  const { start, end, timeRangeType } = query;
  const newStart: Date = new Date(start);
  const newEnd: Date = new Date(end);

  if (timeRangeType === TimeRangeType.Month) {
    newStart.setMonth(0);
    newStart.setDate(0);
    newStart.setHours(0, 0, 1, 1);
    newEnd.setFullYear(newEnd.getFullYear() + 1);
    newEnd.setMonth(0);
    newEnd.setDate(0);
    newEnd.setUTCHours(0, 0, 1, 0);
    newEnd.setSeconds(newEnd.getSeconds() - 2);
  } else if (timeRangeType === TimeRangeType.Year) {
    newStart.setFullYear(newStart.getFullYear() - 2);
    newStart.setMonth(0);
    newStart.setDate(0);
    newStart.setHours(0, 0, 1, 1);

    newEnd.setFullYear(newStart.getFullYear() + 2);
    newEnd.setFullYear(newEnd.getFullYear() + 1);
    newEnd.setMonth(0);
    newEnd.setDate(0);
    newEnd.setUTCHours(0, 0, 1, 0);
    newEnd.setSeconds(newEnd.getSeconds() - 2);
  }

  return [newStart, newEnd];
};
