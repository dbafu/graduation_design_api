import { Module } from '@nestjs/common';
import { MulterConfigService } from './multer-config.service';
import { FileController } from './file.controller';
import { MulterModule } from '@nestjs/platform-express';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule,
    MulterModule.registerAsync({
      useClass: MulterConfigService,
      imports: [ConfigModule],
      inject: [ConfigService],
    }),
  ],
  providers: [ConfigService],
  controllers: [FileController],
})
export class FileModule {}
