import {
  Controller,
  Get,
  Param,
  Post,
  Req,
  Res,
  UploadedFile,
  //   UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';

import * as path from 'path';
import { Response, Request } from 'express';

import {
  //   FileFieldsInterceptor,
  FileInterceptor,
  //   FilesInterceptor,
} from '@nestjs/platform-express';
import { ReqContext } from 'src/shared/request-context/req-context.decorator';
import { RequestContext } from 'src/shared/request-context/request-context.dto';
import { ConfigService } from '@nestjs/config';
import { Public } from '../auth/decorators/public.decorator';

@Controller('file')
export class FileController {
  constructor(private readonly configServise: ConfigService) {}

  @Public()
  @Get('icon/:id')
  async getIconFile(
    @ReqContext() _ctx: RequestContext,
    @Param('id') id: string,
    @Res() res: Response,
  ) {
    const projectRootDir = path.resolve(__dirname, '../../..');
    // console.log('--------projectRootDir: ', projectRootDir);

    const destDir = this.configServise.get('UPLOAD.DEST');

    const UPLOAD_DIR = path.join(projectRootDir, destDir);

    // console.log('----------UPLOAD_DIR: ', UPLOAD_DIR);
    // console.log('---id: ', id);

    const filePath = `${UPLOAD_DIR}/icon/${id}`;
    // console.log('-----filePath: ', filePath);
    console.log('-----filePath file type: ', filePath);

    res.sendFile(filePath);
    // res
  }

  @Public()
  @Get(':id')
  async getFile(
    @ReqContext() _ctx: RequestContext,
    @Param('id') id: string,
    @Res() res: Response,
  ) {
    const projectRootDir = path.resolve(__dirname, '../../..');
    // console.log('--------projectRootDir: ', projectRootDir);

    const destDir = this.configServise.get('UPLOAD.DEST');

    const UPLOAD_DIR = path.join(projectRootDir, destDir);

    // console.log('----------UPLOAD_DIR: ', UPLOAD_DIR);
    // console.log('---id: ', id);

    const filePath = `${UPLOAD_DIR}/${id}`;
    // console.log('-----filePath: ', filePath);
    console.log('-----filePath file type: ', filePath);

    res.sendFile(filePath);
    // res
  }

  @Post('upload1')
  @UseInterceptors(FileInterceptor('file', { preservePath: true }))
  uploadFile(@UploadedFile() file: Express.Multer.File) {
    console.log('-file--Controller: ', file);

    return {
      data: {
        file: file.filename,
      },
      meta: {},
    };
  }

  @Post('upload2')
  // @UseInterceptors(FileInterceptor('file', { preservePath: true }))
  uploadFile2(@UploadedFile() file: Express.Multer.File, @Req() req: Request) {
    console.log('-file--Controller: ', file);
    console.log('-file--Controller req: ', req);

    return {
      data: {
        file: file.filename,
      },
      meta: {},
    };
  }

  //   https://docs.nestjs.cn/10/techniques?id=%e6%96%87%e4%bb%b6%e4%b8%8a%e4%bc%a0

  //   @Post('upload2')
  //   @UseInterceptors(FilesInterceptor('files'))
  //   uploadFiles(@UploadedFiles() files: Array<Express.Multer.File>) {
  //     console.log(files);
  //   }

  //   @Post('upload3')
  //   @UseInterceptors(
  //     FileFieldsInterceptor([
  //       { name: 'avatar', maxCount: 1 },
  //       { name: 'background', maxCount: 1 },
  //     ]),
  //   )
  //   uploadMultiFile(
  //     @UploadedFiles()
  //     files: {
  //       avatar?: Express.Multer.File[];
  //       background?: Express.Multer.File[];
  //     },
  //   ) {
  //     console.log(files);
  //   }
}
