import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';

import { STRATEGY_JWT_AUTH } from '../constants/strategy.constant';
import { Reflector } from '@nestjs/core';
import { IS_PUBLIC_KEY } from '../decorators/public.decorator';

@Injectable()
export class JwtAuthGuard extends AuthGuard(STRATEGY_JWT_AUTH) {
  constructor(private reflector: Reflector) {
    super();
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    // Add your custom authentication logic here
    // for example, call super.logIn(request) to establish a session.
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    console.log('----JwtAuthGuard----isPublic: ', isPublic);

    const request = context.switchToHttp().getRequest();
    console.log('---request url: ', request.url);
    // console.log(request);

    if (isPublic) {
      // 💡 See this condition
      return true;
    }

    return super.canActivate(context);
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  handleRequest(err: any, user: any, info: any) {
    // You can throw an exception based on either "info" or "err" arguments
    console.log('-----JwtAuthGuard-err: ', err);
    console.log('-----JwtAuthGuard-user: ', user);

    if (err || !user) {
      console.log(
        '--JwtAuthGuard---handleRequest--: ',
        err || new UnauthorizedException(`${info}`),
      );

      // throw err || new UnauthorizedException(`${info}`);
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
