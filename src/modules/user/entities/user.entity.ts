import { Finance } from 'src/modules/finance/entities/finance.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  // OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';

// import { Article } from '../../article/entities/article.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100, nullable: true })
  name: string;

  @Column()
  password: string;

  @Unique('username', ['username'])
  @Column({ length: 200 })
  username: string;

  @Column({ type: 'simple-array', nullable: true })
  roles: string[];

  @Column({ nullable: true })
  isAccountDisabled: boolean;

  // @Unique('email', ['email'])
  @Column({ length: 200, nullable: true })
  email: string;

  @CreateDateColumn({ name: 'createdAt', nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedAt', nullable: true })
  updatedAt: Date;

  @OneToMany(() => Finance, (finance) => finance.user)
  finances: Finance[];
}
