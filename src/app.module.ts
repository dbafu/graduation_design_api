import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SharedModule } from './shared/shared.module';
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';
// import { ArticleModule } from './modules/article/article.module';
import { FinanceModule } from './modules/finance/finance.module';
import { UsageModule } from './modules/usage/usage.module';
import { FileModule } from './modules/file/file.module';
import { HttpModule } from '@nestjs/axios';

@Module({
  // imports: [SharedModule, UserModule, AuthModule, ArticleModule],
  imports: [
    SharedModule,
    UserModule,
    AuthModule,
    FinanceModule,
    UsageModule,
    FileModule,
    HttpModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
