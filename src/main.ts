import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
// import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RestResponseTransformInterceptor } from './shared/interceptors/rest-response-transform.interceptor';
// import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.useGlobalInterceptors(new RestResponseTransformInterceptor());

  // app.useStaticAssets(join(__dirname, '..', 'upload'));

  const configService = app.get(ConfigService);
  const PORT = configService.get<number>('APP.PORT') || 3000;

  await app.listen(PORT, () => {
    console.log('app is serving on PORT: ' + PORT);
  });
}
bootstrap();
